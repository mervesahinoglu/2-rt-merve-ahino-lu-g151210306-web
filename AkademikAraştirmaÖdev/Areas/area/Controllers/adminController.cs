﻿using AkademikAraştirmaÖdev.Areas.area.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AkademikAraştirmaÖdev.Areas.area.Controllers
{
    public class adminController : Controller
    {
        AraştırmaEntities db = new AraştırmaEntities();
        [Authorize(Users = "artemiswolfs@gmail.com")]
        public ActionResult kitap()
        {
            var model = db.kitap.ToList();
            return View( model);
        }
        public ActionResult Add()
        {
          
            return View();
        }
        const string  imageFolderPath = "~/temaadm/resim/";
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public ActionResult KitapAdd(Class1 model)
        {
            string filename = string.Empty;
            if (model.resim != null && model.resim.ContentLength > 0)
            {
     
                filename = model.resim.FileName;
                var path = Path.Combine(Server.MapPath(imageFolderPath), filename);
                model.resim.SaveAs(path);

            }

            if (!ModelState.IsValid)
            {
                            
                kitap kitap = new kitap();
                kitap.kitapadı = model.kitapadı;
                kitap.kitapaçık = model.kitapaçık;
                kitap.kitaptarihi = DateTime.Now;
                kitap.kitaptürü = model.kitaptürü;
                kitap.kitaplink = imageFolderPath + filename;
                
                db.kitap.Add(kitap);
                db.SaveChanges();

            }
            return RedirectToAction("kitap");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
             
            return View(db.kitap.Find(id));
        }
        [HttpPost]
        public ActionResult Edit(kitap model)
        {
            if (ModelState.IsValid)
            {

 
            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            }
            return RedirectToAction("kitap");
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            return View(db.kitap.Find(id));
        }
        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {
            db.kitap.Remove(db.kitap.Find(id));
            db.SaveChanges();
            return RedirectToAction("kitap");
        }
    }
   
    }