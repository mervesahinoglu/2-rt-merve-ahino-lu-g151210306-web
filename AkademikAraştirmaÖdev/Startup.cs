﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AkademikAraştirmaÖdev.Startup))]
namespace AkademikAraştirmaÖdev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
