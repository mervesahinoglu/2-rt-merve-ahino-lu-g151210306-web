﻿using AkademikAraştirmaÖdev.Areas.area.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AkademikAraştirmaÖdev.Controllers
{
  
    public class HomeController : Controller
    {
        AraştırmaEntities db = new AraştırmaEntities();

        public ActionResult Index()
        {
   
            return View();
        }
        public ActionResult Giriş()
        {
            return View(db.kitap.ToList());
        }
        public ActionResult felsefe()
        {

            var model = db.kitap.ToList();
            return View(model);
         
        }

    }
}